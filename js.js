const form = document.querySelector('form');
const inputs = document.querySelectorAll('.input-div input');

function criarErro(input, mensagem) {
    const divPai = input.parentElement;
    let imgErro = document.createElement('img');
    let pErro = document.createElement('p');

    pErro.innerText = mensagem;
    imgErro.setAttribute('src', 'img/icon-error.svg');

    let duplicaTexto = divPai.querySelector('p');
    let duplicaImg = divPai.querySelector('img');

    if (!duplicaTexto && !duplicaImg) {
        divPai.appendChild(pErro);
        divPai.appendChild(imgErro);
    }
}

function removeErro(input) {
    const divPai = input.parentElement;
    const imgRemove = divPai.querySelector('img');
    const pRemove = divPai.querySelector('p');

    if (imgRemove && pRemove) {
        imgRemove.remove();
        pRemove.remove();
    }
}

function validarEmail(email) {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
}

form.addEventListener('click', (e) => {
    e.preventDefault();

    inputs.forEach((input) => {
        if (input.value === '') {
            input.classList.add('erro');
            criarErro(input, 'Cannot be empty');
        } else {
            input.classList.remove('erro');
            removeErro(input);

            if (input.type === 'email' && !validarEmail(input.value)) {
                input.classList.add('erro');
                criarErro(input, 'Looks like this is not an email');
            }
        }
    });
});
